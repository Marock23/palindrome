function isPalindrome(palindromeValue) {
    palindromeArray = palindromeValue.replace(/\s/g, "").split("");
    return palindromeArray.join() == palindromeArray.reverse().join();
}

