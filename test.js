
  QUnit.module('Palindrome', function() {
    QUnit.test('Palindrome', function(assert) {
      assert.equal(isPalindrome("laval"), true, "Laval est un palindrome");
      assert.equal(isPalindrome("patate"), false, "patate n'est pas un palindrome");
    });
  });